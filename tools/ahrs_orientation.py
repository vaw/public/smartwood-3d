"""Orientation estimation using the AHRS algorithm.

This script takes in a merged dataset of sensor data and runs the AHRS
algorithm on it. If the "Sensor active" flag (column 19) is false, it
is assumed that the sensor is not moving and the last orientation is
used.
"""

import argparse
import csv
import pathlib
import typing

import numpy as np

try:
    import smartwood
except ImportError as err:
    # When running this script from the repository root without having the
    # project installed, the import might fail - the below tries again using
    # the relative path to the source code.
    import sys
    import os
    sys.path.append(os.getcwd())
    try:
        import smartwood
    except ImportError:
        raise err from err
finally:
    from smartwood import processing
    from smartwood.processing.utils import quaternion_to_euler
    from smartwood.typing import NDArray, TimeArray


def _load_stitched(path: typing.Union[str, pathlib.Path]
                   ) -> tuple[TimeArray, NDArray]:
    # Convert the CSV file to NumPy arrays
    with open(path, 'r', newline='') as f:
        reader = csv.reader(f)

        # Discard header row
        iterator = iter(reader)
        _ = next(iterator)

        # Process the rest of the file
        times_raw: typing.List[np.datetime64] = []
        data_raw: typing.List[NDArray] = []
        for row in reader:
            times_raw.append(np.datetime64(row[0]))
            data_raw.append(np.array([float(x)  # type: ignore
                            for x in row[1:]]))

    times: TimeArray = np.array(times_raw)  # type: ignore
    data: NDArray = np.array(data_raw)  # type: ignore
    return times, data


def main(path: typing.Union[str, pathlib.Path],
         output: typing.Union[str, pathlib.Path]) -> None:
    """Main script function."""

    if not isinstance(path, pathlib.Path):
        path = pathlib.Path(path)
    times, data = _load_stitched(path)
    orientation: NDArray = np.zeros((len(data), 4))  # type: ignore

    # Every time a "real" dataset starts (i.e. the "active sensor" flag goes
    # from 0 to 1), we'll start a new temporary recording for the duration of
    # that recording.
    # NOTE: This is simply due to the orientation logic expecting a recording
    # object rather than a raw dataset; this may be changed in future versions.
    last_orientation: typing.Optional[NDArray] = None
    start_index: typing.Optional[int] = None
    for index, _ in enumerate(data):

        # Find start of a recording
        if data[index, 9] == 1 and start_index is None:
            start_index = index

        # Find end of a recording
        elif data[index, 9] == 0 and start_index is not None:

            # Create a recording object from this range
            rec_times = times[start_index:index]
            rec_data = data[start_index:index, :9]
            rec = smartwood.Recording(rec_times[0], rec_times, rec_data)

            rec_orientation = processing.get_orientation(
                rec, initial=last_orientation)
            orientation[start_index:index, :] = rec_orientation

            last_orientation = rec_orientation[-1, :]
            # Reset the start index
            start_index = None

        # If we're not in a recording, just copy the last orientation
        if last_orientation is not None:
            orientation[index, :] = last_orientation
        else:
            orientation[index, :] = np.nan

    angles: NDArray = np.empty((orientation.shape[0], 3))  # type: ignore
    for index, quat in enumerate(orientation):
        angles[index, :] = quaternion_to_euler(quat, True)

    # Export the data
    if output is None:
        output = path.parent / 'orientation.csv'
    with open(output, 'w', newline='') as f:
        writer = csv.writer(f)
        writer.writerow([
            'Timestamp',
            'Magnetometer X [uT]', 'Magnetometer Y [uT]', 'Magnetometer Z [uT]',
            'Gyroscope X [deg/s]', 'Gyroscope Y [deg/s]', 'Gyroscope Z [deg/s]',
            'Accelerometer X [mg]', 'Accelerometer Y [mg]',  'Accelerometer Z [mg]',
            'Orientation X [deg]', 'Orientation Y [deg]', 'Orientation Z [deg]',
            'Sensor active'])
        for index, row in enumerate(data):
            writer.writerow([
                times[index],
                *row[:9],
                *angles[index, :],
                int(row[9])])


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('path', type=str,
                        help='Path to the stitched CSV file to read.')
    parser.add_argument('-o', '--output', type=str, default=None,
                        help='Path to the output CSV file to write.')

    args = parser.parse_args()
    main(args.path, args.output)
