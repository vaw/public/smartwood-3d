"""Script for estimating smoothing parameters for a given dataset.

This script takes in a single recording of a SmartWood sensor sitting
idle with no movement and tries different moving average sample counts.

Fewer samples are more accurate and respond more quickly, but also
retain more noise.
"""

import argparse
import os
import typing

import matplotlib.pyplot as plt


try:
    import smartwood
except ImportError as err:
    # When running this script from the repository root without having the
    # project installed, the import might fail - the below tries again using
    # the relative path to the source code.
    import sys
    import os
    sys.path.append(os.getcwd())
    try:
        import smartwood
    except ImportError:
        raise err from err
from smartwood.typing import NDArray
from smartwood.processing import moving_average


def get_mean_squared_error(data: NDArray, samples: int) -> float:
    total = 0.0
    for coord in range(data.shape[1]):
        average = data[:, coord].mean()
        total += abs(((moving_average(
            data[:, coord], samples) - average) ** 2).mean() / average)
    return total


def main(filepath: str, min_: int = 1, max_: int = 25) -> None:
    rec = smartwood.load_recording(filepath)

    # Set up shared plot
    fig, ax = plt.subplots(3, sharex=True)
    fig.suptitle('Mean squared error by moving average sample counts')
    fig.supxlabel('Moving average sample count')
    fig.supylabel('Mean squared error')
    fig.set_size_inches(8, 6)

    # Separate guesses for each sensor
    fields = ('accelerometer', 'gyroscope', 'magnetometer')
    for index, name in enumerate(fields):
        data = getattr(rec, name)
        # Run the sample count tester for each sample count
        sample_counts = list(range(min_, max_+1))
        sample_errors: typing.List[float] = []
        for samples in sample_counts:
            error = get_mean_squared_error(data, samples)
            sample_errors.append(error)

        axis: typing.Any = ax[index]  # type: ignore
        axis.plot(sample_counts, sample_errors, 'o-')
        axis.set_title(name)
        axis.grid()

    # Save and clear th eplot
    plt.savefig(f'sensor_smoothing.png')
    plt.clf()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('filepath', type=str,
                        help='Path to the converted Smartwood Recording CSV.')

    args = parser.parse_args()
    main(args.filepath)
