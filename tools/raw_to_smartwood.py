"""Sensor output converter from raw to SmartWood format.

This script converts the raw data exported from the sensor USB dongle
into the generalised format used in the SmartWood project.
"""

import argparse
import pathlib
import typing

import numpy as np

try:
    import smartwood
except ImportError as err:
    # When running this script from the repository root without having the
    # project installed, the import might fail - the below tries again using
    # the relative path to the source code.
    import sys
    import os
    sys.path.append(os.getcwd())
    try:
        import smartwood
    except ImportError:
        raise err from err
finally:
    from smartwood.typing import NDArray


def convert_dataset(filepath: typing.Union[str, pathlib.Path],
                    output_dir: typing.Union[str, pathlib.Path] = '.',
                    encoding: str = 'utf-8',
                    ext_year: int = 0,
                    tolerant: bool = False) -> None:
    if tolerant:
        print('Parsing in high tolerance mode')
        imported = smartwood.parse_txt_tolerant(filepath, encoding, ext_year)
    else:
        imported = smartwood.parse_txt(filepath, encoding, ext_year)

    # The coordinate system used depends on the sensor and is different from
    # the SmartWood coordinate system. We must therefore flip some axes and
    # rotate some columns about.
    converted = smartwood.io.Dataset(imported.sensor_name)
    for recording in imported.recordings():
        data: NDArray = np.empty(  # type: ignore
            (recording.timestamps.shape[0], 9))

        # MAGNETOMETER
        data[:, 0] = -recording.magnetometer[:, 1]
        data[:, 1] = -recording.magnetometer[:, 0]
        data[:, 2] = -recording.magnetometer[:, 2]

        # GYROSCOPE
        data[:, 3] = -recording.gyroscope[:, 1]
        data[:, 4] = -recording.gyroscope[:, 0]
        data[:, 5] = -recording.gyroscope[:, 2]

        # ACCELEROMETER
        data[:, 6] = -recording.accelerometer[:, 1]
        data[:, 7] = -recording.accelerometer[:, 0]
        data[:, 8] = -recording.accelerometer[:, 2]
        converted.add_recording(smartwood.io.Recording(
            recording.start, recording.timestamps, data))

    smartwood.save_dataset(converted, output_dir)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('filepath', type=str,
                        help='Path to the raw sensor data TXT file.')
    parser.add_argument('-o', '--output-dir', type=str, default='.',
                        help='Path to the output directory.')
    parser.add_argument('-e', '--encoding', type=str, default='utf-8',
                        help='Encoding of the input file.')
    parser.add_argument('-y', '--ext-year', type=int, default=0,
                        help='External year input for all timestamps.')
    parser.add_argument('-t', '--tolerant', action='store_true',
                        help='Parse sensor data in high tolerance mode. This '
                        'accepts most malformed data, but will also destroy '
                        'per-trigger recordings; instead merging all values '
                        'into a single recording. Only enable if default '
                        '(strict) parsing fails with errors.')

    args = parser.parse_args()
    convert_dataset(args.filepath, args.output_dir, args.encoding,
                    args.ext_year, args.tolerant)
