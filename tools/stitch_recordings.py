"""Recording smoothing and stitching utility.

This script takes in a directory of sensor recordings and stitches them
together into a single dataset. The resulting dataset can also be
smoothed to reduce noise.
"""

import argparse
import csv
import pathlib
import typing

try:
    import smartwood
except ImportError as err:
    # When running this script from the repository root without having the
    # project installed, the import might fail - the below tries again using
    # the relative path to the source code.
    import sys
    import os
    sys.path.append(os.getcwd())
    try:
        import smartwood
    except ImportError:
        raise err from err
finally:
    from smartwood import processing


def _get_recordings_in_directory(filepath: typing.Union[str, pathlib.Path]
                                 ) -> typing.List[smartwood.Recording]:
    """Load all recordings from the given directory."""
    recordings: typing.List[smartwood.Recording] = []

    # Load the sensor data
    for filename in pathlib.Path(filepath).iterdir():
        if filename.is_file() and filename.suffix == '.csv':
            recordings.append(smartwood.load_recording(filename))

    return recordings


def main(path: typing.Union[str, pathlib.Path], smoothing: int,
         output: typing.Union[str, pathlib.Path, None]) -> None:
    """Main script function."""

    if not isinstance(path, pathlib.Path):
        path = pathlib.Path(path)
    # Load the sensor data
    recordings = _get_recordings_in_directory(path)

    # Stitch the recordings together
    times, data = processing.stitch(
        recordings, sample_rate=100, smoothing=smoothing)

    # Write stitched output to a CSV file
    if output is None:
        output = path.parent / 'stitched.csv'
    with open(output, 'w', newline='') as f:
        writer = csv.writer(f)
        writer.writerow([
            'Timestamp',
            'Magnetometer X [uT]', 'Magnetometer Y [uT]', 'Magnetometer Z [uT]',
            'Gyroscope X [deg/s]', 'Gyroscope Y [deg/s]', 'Gyroscope Z [deg/s]',
            'Accelerometer X [mg]', 'Accelerometer Y [mg]',  'Accelerometer Z [mg]',
            'Sensor active'])
        for t, d in zip(times, data):
            writer.writerow([t, *d])


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('directory', type=str,
                        help='Path to the directory containing the individual '
                        'recorings.')
    parser.add_argument('-s', '--smoothing', type=int, default=10,
                        help='Smoothing window size.')
    parser.add_argument('-o', '--output', type=str, default=None,
                        help='Path to the output CSV file.')

    args = parser.parse_args()
    main(args.directory, args.smoothing, args.output)
