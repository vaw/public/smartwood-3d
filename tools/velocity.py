"""Calculate local and global velocity from orientation data."""

import argparse
import csv
import pathlib
import typing

import numpy as np
from scipy.spatial.transform import Rotation as sp_rotation  # type: ignore

try:
    import smartwood
except ImportError as err:
    # When running this script from the repository root without having the
    # project installed, the import might fail - the below tries again using
    # the relative path to the source code.
    import sys
    import os
    sys.path.append(os.getcwd())
    try:
        import smartwood
    except ImportError:
        raise err from err
finally:
    from smartwood.processing import get_velocity
    from smartwood.processing.utils import get_dt_from_timestamps
    from smartwood.typing import NDArray, TimeArray

GRAVITY = 9.80665  # m/s^2


def _load_orientation(path: typing.Union[str, pathlib.Path]
                      ) -> tuple[TimeArray, NDArray]:
    with open(path, 'r', newline='') as file_:
        reader = csv.reader(file_)

        # Discard header row
        iterator = iter(reader)
        _ = next(iterator)

        # Process the rest of the file
        times_raw: typing.List[np.datetime64] = []
        data_raw: typing.List[NDArray] = []
        for row in reader:
            times_raw.append(np.datetime64(row[0]))
            data_raw.append(np.array([float(x)  # type: ignore
                            for x in row[1:]]))

    times: TimeArray = np.array(times_raw)  # type: ignore
    data: NDArray = np.array(data_raw)  # type: ignore
    return times, data


def plot_xyz(time: TimeArray, data: NDArray, title: str) -> None:
    import matplotlib.pyplot as plt
    plt.plot(time, data[:, 0], 'tab:red', label='X')
    plt.plot(time, data[:, 1], 'tab:green', label='Y')
    plt.plot(time, data[:, 2], 'tab:blue', label='Z')
    plt.grid()
    plt.legend()
    plt.savefig(f'{title}.png')
    plt.clf()


def main(path: typing.Union[str, pathlib.Path], use_local_z: bool = False,
         start: int = 0, output: typing.Union[str, pathlib.Path, None] = None,
         g_comp: float = 0) -> None:
    """Main script function."""
    if not isinstance(path, pathlib.Path):
        path = pathlib.Path(path)

    # Load data
    times, data = _load_orientation(path)
    # Determine time step
    dt = get_dt_from_timestamps(times)

    gravity = GRAVITY * (1 + g_comp)

    # Get velocity in local coordinates through integration
    rec = smartwood.Recording(times[0], times, data)
    local_vel: NDArray = np.empty((len(times), 3))  # type: ignore
    local_vel[0, :] = np.nan
    local_vel = get_velocity(rec)

    # If use_local_z is True, then the local z-axis is used to account for
    # gravity.
    if use_local_z:
        local_vel[:, 2] += [gravity * dt * i for i in range(len(local_vel))]

    # Create a copy of the acceleration data with IMU orientation applied
    rot = sp_rotation.from_euler(  # type: ignore
        'xyz', data[:, 9:12], degrees=True)
    global_acc: NDArray = rot.apply(data[:, 6:9])  # type: ignore

    # Create a new recording to calculate global velocity with
    rec_data = np.concatenate(  # type: ignore
        (data[:, 0:6], global_acc), axis=1)
    rec = smartwood.Recording(times[0], times[start:], rec_data[start:, :])

    # Calculate global velocity, ignoring the first `start` samples
    global_vel: NDArray = np.empty((len(times), 3))  # type: ignore
    global_vel[:start+2, :] = np.nan
    global_vel[start+1:, :] = get_velocity(rec)

    if not use_local_z:
        global_vel[:, 2] -= [
            gravity * dt * (i-start-1) if i > start+1 else 0.0
            for i in range(len(global_vel))]

    # Export the data
    if output is None:
        output = path.parent / 'velocity.csv'
    with open(output, 'w', newline='') as f:
        writer = csv.writer(f)
        writer.writerow([
            'Timestamp',
            'Magnetometer X [uT]', 'Magnetometer Y [uT]', 'Magnetometer Z [uT]',
            'Gyroscope X [deg/s]', 'Gyroscope Y [deg/s]', 'Gyroscope Z [deg/s]',
            'Accelerometer X [mg]', 'Accelerometer Y [mg]',  'Accelerometer Z [mg]',
            'Orientation X [deg]', 'Orientation Y [deg]', 'Orientation Z [deg]',
            'Sensor active',
            'Local velocity X [m/s]', 'Local velocity Y [m/s]', 'Local velocity Z [m/s]',
            'Global velocity X [m/s]', 'Global velocity Y [m/s]', 'Global velocity Z [m/s]',
        ])
        for index, row in enumerate(data[1:]):
            writer.writerow([
                times[1+index],
                *row,
                *local_vel[index, :],
                *global_vel[index, :]])


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('path', type=str,
                        help='Path to the stitched CSV file to read.')
    parser.add_argument('-s', '--start', type=int, default=200,
                        help='Start offset for velocity data. Used to skip '
                        'the first few seconds of data where orientation '
                        'is not yet stable. Value is in samples.')
    parser.add_argument('--use-local-z', action='store_true', default=False,
                        help='Use the local z-axis to account for gravity.')
    parser.add_argument('-o', '--output', type=str, default=None,
                        help='Path to the output CSV file to write.')
    parser.add_argument('-g', '--gravity', type=float, default=0.0,
                        help='Gravity correction factor. Default is 0.0. A '
                        'value of 0.035 would increase gravity by 3.5%%, a '
                        'value of -0.02 would decrease it by 2%%.')

    args = parser.parse_args()
    main(args.path, args.use_local_z, args.start, args.output, args.gravity)
