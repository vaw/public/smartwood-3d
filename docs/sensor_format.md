# Sensor Output Format

The following is a reference to the output format used by the sensors and has been abridged from the SST Sensor Handbook v121 (Section 7 - Recorded data files). It acts as an implementation reference for the I/O components of SmartWood utilities.

## Format Configuration

This software expects timestamps to be encoded in the MS Excel format, i.e. `year;day-of-year;time;milliseconds`.

## 1. Common Message Blocks

### 1.1 Timestamps

Depending on the export configuration selected in the SST software, the number of fields written for a given timestamp may vary.

This implementation always expects MS Excel style timestamps consisting of four fields:

    year;day-of-year;time;milliseconds

### 1.2 Logging Config

Some blocks contain a declaration of logging configuration in their message, generally right after their unique identifier:

    206;                                    // Sensor ID
    ...
    <Timestamp>
    ... 
    recorded_data:9dof;                     // Type of data
    fps_acc:100.0;                          // Accelerometer sample rate
    fps_gyro:100.0;                         // Gyroscope sample rate
    fps_ecomp:100.0;                        // Magnetometer sample rate
    mode:25                                 // Sensor mode

### 1.3 Engineering Data

Additional metadata information used for troubleshooting or introspection. Notably, this does contain temperature information, which could be used to account for temperature drift given known sensor curves.

#### 1.3.1 Line 1

    E_data:206;                             // Sensor name
    Temperature:18.52°C;                    // Temperature
    BAT_AFTER_BMM:3.86V;                    // Battery levels
    BAT_AFTER_16BRF:3.86V                   // ^

#### 1.3.2 Line 2

    E_data2:206;                            // Sensor name
    BAT_AFTER_SLEEP:4.23V;                  // Battery level
    VDD_AFTER_BMA_SLEEP_DURING_WFT:6.69V    // Regulated rail voltage

## 2. Message Types

### 2.1 `meas_started`

Signifies the start of a sensor measurement session. Aside from information about the sensor and timestamp, this also contains information about the logging configuration itself, in particular the accelerometer thresholds used to trigger data recording.

    meas_started;                           // Message type tag
    ...
    <Logging Config>
    ...
    start_th:24mg;                          // Logging start threshold in mg
    stop_th:64mg;                           // Logging stop threshold in mg
    meas_timeout:540sec;                    // Logging stop hysteresis in ms (?)
    g_range:16;                             // ?
    rf_msg:0;                               // ?
    gps:3;                                  // ?

### 2.2 `trig_start_record`

Start block of a stream of sensor data. When the triggering conditions for a sensor are met as defined by the accelerometer threshold level, this block is authored.

It will first flush any pre-triggering values that are still available in the buffer to disk (`VALUE_BEFORE_TRIGGER` tag) before writing new rows (`VALUE` tag). The triggering timestamp lies between the last `VALUE_BEFORE_TRIGGER` and the first `VALUE` entry.

#### 2.2.1 Lines 1 and 2

    ...
    <Engineering data lines 1 and 2>
    ...

#### 2.2.2 Line 3

    meas_started;                           // Message type tag
    ...
    <Logging Config>
    ...

#### 2.2.3 Line 4

    number_of_frames:443;                   // Number of samples
    number_of_frames_before_trigger:50;     // Number of samples prior to trigger event
    time_per_frame:10.000ms;                // Target logging interval
    time per frame actual:10.63417ms        // Actual logging interval

The `time_per_frame` vs. `time per frame actual` discrepancy is due to drift between the microcontrollers internal clock and the RTC (real time clock) of the sensor.

Note that the `number_of_frames` field does not match the actual number of frames written. This is likely a side effect of the mismatch between the `time_per_frame` and `time per frame actual` fields, but this is not covered explained in the documentation.

To determine the actual number of frames, keep reading lines until a `rec_stopped` block is encountered (recognizable by a line not starting with `VALUE`).

### 2.3 `rec_stopped`

End block of a stream of sensor data. Contains another block of engineering data, most crucially another temperature measurement.

#### 2.3.1 Lines 1 and 2

    ...
    <Enginnering data lines 1 and 2>
    ...

#### 2.3.2 Line 3

    rec_stopped;
    ...
    <Logging Config>
    ...

### 2.4 Recorded Data

Any rows between the `trig_start_record` and `rec_stopped` tags can be expected to be bulk data insertion.

    VALUE;                                      // Value type tag
    ...
    <Timestamp>
    ...
    BMMx:27.9uT;                                // Magnetometer readout
    BMMy:-9.0uT;                                // ^
    BMMz:-42.6uT;                               // ^
    GYRx:-0.31°/s;                              // Gyrosope readout
    GYRy:-0.24°/s;                              // ^
    GYRz:-16.66°/s;                             // ^
    ACCx:-31.25mG;                              // Accelerometer readout
    ACCy:-10.25mG;                              // ^
    ACCz:1000.98mG                              // ^

The first value, labelled "value type tag" here, will either be the string `VALUE`, or `VALUE_BEFORE_TRIGGER`. The latter signifies that this value has been read from buffer and was recorded immediately before data recording was triggered. The expected number of pre-trigger data points can be inferred from the `num_frames_before_trigger` field in the [`trig_start_record`](#22-trigstartrecord) block.

### 2.5 `meas_ended`

Marks the end of a logging session.

    meas_ended;                           // Message type tag
    ...
    <Logging Config>
    ...

### 2.6 Static / Reference Data

Regardless of whether a measurement was recorded, the log file may also contain one or more `static_values` entries.

These are reference / calibration data taken in regular intervals regardless of whether a data recording trigger was observed and are placed after a `meas_ended` block, usually located at the end of the file.

    static_values;                              // Message type tag
    21;185;11:49:42;323;                        // Timestamp
    BMMx:-5.4uT;BMMy:-7.8uT;BMMz:11.1uT;        // Magnetometer readout
    ACCx:350.59mG;ACCy:-21.48mG;ACCz:-980.96mG; // Accelerometer readout
    intTemp:18.48°C                             // Internal temperature
