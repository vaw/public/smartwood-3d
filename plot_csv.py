import csv
import typing

import numpy as np
import matplotlib.pyplot as plt
from smartwood.typing import TimeArray, NDArray


def seconds_from_datetime(times: TimeArray) -> NDArray:
    """Convert an array of timestamps into seconds.

    The first timestamp will be subtracted from every subsequent
    timestamp to calculate the difference in seconds.
    """
    seconds: NDArray = np.empty(times.shape, dtype=np.float32)  # type: ignore
    if len(times) < 1:
        seconds[0] = 0.0
        return seconds
    first: np.datetime64 = times[0]
    for index, timestamp in enumerate(times):
        timedelta: np.timedelta64 = timestamp - first
        seconds[index] = timedelta / np.timedelta64(1, 's')
    return seconds


def _read_csv(path: str) -> typing.Tuple[typing.List[str], TimeArray, NDArray]:
    with open(path, 'r', newline='') as file_:
        reader = csv.reader(file_)
        headers = next(reader)
        times_raw = []
        data_raw = []
        for row in reader:
            times_raw.append(np.datetime64(row[0]))  # type: ignore
            data_raw.append([float(s) for s in row[1:]])  # type: ignore
    time: TimeArray = np.array(times_raw).astype(np.datetime64)  # type: ignore
    data: NDArray = np.array(data_raw).astype(np.float32)  # type: ignore
    return headers, time, data


if __name__ == '__main__':

    # Specify the input CSV file to load.
    PATH = 'C:/path/to/dataset.csv'

    _, csv_timestamps, csv_data = _read_csv(PATH)

    # Specify the columns to plot. The list below is only valid for the output
    # of velocity.py.
    # Check the CSV file headers to see which columns are available. Note that
    # The first column is parsed separately and is not part of the csv_data
    # array.
    # ############################
    # Index 0-2: Magnetometer
    # Index 3-5: Gyroscope
    # Index 6-8: Accelerometer
    # Index 9-11: Orientation
    # Index 12: Sensor active (0 or 1)
    # Index 13-15: Velocity (local)
    # Index 16-18: Velocity (global)

    # Plot time in seconds since start
    data_x = seconds_from_datetime(csv_timestamps)

    # Plot sensor orientation along Y (second index is exclusive)
    data_y = csv_data[:, 9:12]
    # Plot a single value instead (X orientation only)
    # data_y = csv_data[:, 9]

    #######################################

    # Set the plot width and height in inches
    plt.figure(figsize=(8, 5))

    # If more than one column was specified, plot each as a separate line
    if len(data_y.shape) > 1:
        # Specify the colours to use for each axis
        colours = ['r', 'g', 'b']
        for index, label in enumerate(('X', 'Y', 'Z')):
            plt.plot(data_x, data_y[:, index], colours[index], label=label)
        # Enable per-line legend
        plt.legend()
    else:
        plt.plot(data_x, data_y)
    # Set plot heading
    plt.title('Sensor Orientation')
    # Enable grid, if desired
    plt.grid()
    # Set x-axis label
    plt.xlabel('Time [s]')
    # Set y-axis label
    plt.ylabel('Angle [deg]')

    # Export the plot as a PNG file with 600 DPI resolution
    plt.savefig(f'plot.png', dpi=600)
    # Export the plot as a PDF file
    # plt.savefig(f'plot.pdf')
    plt.clf()
