import typing
import unittest

from smartwood.log_parser._iterator import PeekableIterator


class PeekableIteratorTests(unittest.TestCase):
    """Test cases for the internal PeekableIterator class."""

    list_: typing.List[int]
    iter_: PeekableIterator[int]

    def setUp(self) -> None:
        super().setUp()
        self.list_ = list(range(1, 11))
        self.iter_ = PeekableIterator(self.list_)

    def tearDown(self) -> None:
        super().tearDown()
        self.list_.clear()

    def test_requires_iter(self) -> None:
        # Exhaust the iterator
        for _ in self.iter_:
            pass
        # Try to peek
        with self.assertRaises(RuntimeError):
            self.iter_.peek(1)

    def test_no_negative_offsets(self) -> None:
        with self.assertRaises(ValueError):
            self.iter_.peek(-1)

    def test_peek_0_returns_next_value(self) -> None:
        # Consume a few values
        for _ in range(3):
            next(self.iter_)
        # Peek to get the upcoming value
        peeked = self.iter_.peek(0)
        next_ = next(self.iter_)
        self.assertEqual(peeked, next_)

    def test_peek_1_does_not_skip(self) -> None:
        # Consume a few values
        for _ in range(5):
            next(self.iter_)
        # Peek ahead by 1
        peeked = self.iter_.peek(1)
        self.assertEqual(peeked, 7)
        self.assertEqual(next(self.iter_), 6)
        self.assertEqual(next(self.iter_), 7)
        self.assertEqual(next(self.iter_), 8)

    def test_multiple_peeks(self) -> None:
        # Consume a few values
        for _ in range(3):
            next(self.iter_)
        # Peek by 2
        self.assertEqual(self.iter_.peek(2), 6)
        # Peek by 4
        self.assertEqual(self.iter_.peek(4), 8)
        # Peek by 1
        self.assertEqual(self.iter_.peek(1), 5)
        # Consume a few
        for _ in range(2):
            next(self.iter_)
        # Peek by 1
        self.assertEqual(self.iter_.peek(1), 7)
