"""Utilities used to stitch sensor recordings together."""

import itertools
import typing

import numpy as np

from ..recording import Recording
from ..typing import NDArray, TimeArray

from .utils import moving_average


def stitch(recordings: typing.List[Recording], sample_rate: int = 100,
           smoothing: int = 10) -> tuple[TimeArray, NDArray]:
    """Create a single merged dataset from multiple input recordings.

    For each provided recording, the data is smoothed using the given
    moving average window size. The smoothed data is then interpolated
    to the given sample rate to synchronize the data across recordings.

    :param recordings: List of recordings to stitch.
    :type recordings: :obj:`list` [:class:`smartwood.Recording`]
    :param sample_rate: Sample rate of the merged dataset in Hz
    :type sample_rate: :class:`int`
    :param smoothing: Moving-average smoothing samples.
    :type smoothing: :class:`int`
    :return: Stitched dataset times and values
    :rtype: :class:`tuple` [:obj:`numpy.ndarray`, :obj:`numpy.ndarray`]
    """
    ts_start, ts_end = _get_start_end_times(recordings)
    length = int(np.ceil((ts_end - ts_start) /
                         np.timedelta64(1, 's') * sample_rate))
    length -= smoothing + 1

    merged_data: NDArray = np.zeros((length, 10))  # type: ignore

    for recording in recordings:
        ts_rec, data = _smooth_and_interpolate(
            recording, sample_rate, smoothing)
        # Determine the appropriate insertion point for the merged dataset
        o = int((ts_rec - ts_start) / np.timedelta64(1, 's') * sample_rate)

        # Extend the merged dataset if necessary
        if merged_data.shape[0] < o + data.shape[0]:
            merged_data.resize((o + data.shape[0], 10))  # type: ignore
        # Insert IMU data
        merged_data[o:o+data.shape[0], :data.shape[1]] = data
        # Set "real data" flag
        merged_data[o:o+data.shape[0], 9] = 1

    # Calculate interpolated timestamps
    timestamps = np.array([  # type: ignore
        ts_start + np.timedelta64(int(i * 1000 / sample_rate), 'ms')
        for i in range(merged_data.shape[0])])

    # Set any fields with no data to NaN
    for index in range(merged_data.shape[0]):
        if not merged_data[index, 9]:
            merged_data[index, :9] = np.nan

    return timestamps, merged_data


def _get_start_end_times(
        data: typing.List[Recording]) -> tuple[np.datetime64, np.datetime64]:
    """Retrieve the first and last timestamp in the given recordings.

    This assumes that the individual recordings' timestamps are
    ordered, the order of the recordings may be arbitrary.

    :param data: List of recordings to scan.
    :type data: :obj:`list` [:class:`smartwood.Recording`]
    :return: First and last timestamp in the given recordings.
    """
    times: typing.List[np.datetime64] = []
    for recording in data:
        times.extend((recording.timestamps[0], recording.timestamps[-1]))
    return np.min(times), np.max(times)  # type: ignore


def _interpolate_via_timestamp(data: NDArray, timestamps: TimeArray,
                               sample_rate: int) -> NDArray:
    """Interpolate the given data based on timestamp.

    The first timestamp will be treated as a reference, with all data
    linearly interpolated into the given sample rate. The dataset will
    be ended once the next value would fall beyond the end of the
    original dataset.

    :param data: 1D array to interpolate.
    :type data: :obj:`np.ndarray`
    :param timestamps: 1D array of timestamps.
    :type timestamps: :obj:`np.ndarray`
    :return: Interpolated values.
    :raises ValueError: If the two arrays are of different lengths.
    """
    if len(timestamps) != len(data):
        raise ValueError('Timestamps and data must be of the same length.')
    if len(timestamps) < 1:
        return np.array([])  # type: ignore

    dt = 1 / sample_rate
    ref = timestamps[0]

    interpol_data: typing.List[float] = []

    last = 0
    for index in itertools.count(1):
        # Get the offset from the first sample in ms
        offset = int(index * dt * 1000)

        # Break if this offset is beyond the end of the dataset
        timestamp = ref + np.timedelta64(offset, 'ms')
        if timestamp >= timestamps[-1]:
            break

        since_last = (timestamp - timestamps[last]) / np.timedelta64(1, 's')
        if since_last == 0:
            continue

        if last+1 >= len(data):
            break

        interpol_data.append(
            (data[last+1] - data[last]) * since_last / dt + data[last])

        if timestamp > timestamps[last]:
            last += 1

    return np.array(interpol_data)  # type: ignore


def _smooth_and_interpolate(recording: Recording, sample_rate: int,
                            smoothing: int) -> tuple[np.datetime64, NDArray]:
    """Smoothe and resample the given recording to a new sample rate.

    Individual recording's timestamps are not uniform and don't align
    with the theoretical sample rate perfectly. This function first
    smoothes the recording's data using a moving average window of the
    given size, then interpolates the data to the given sample rate.

    :param recording: Recording to smooth and interpolate.
    :type recording: :class:`smartwood.Recording`
    :param sample_rate: Sample rate of the interpolated dataset in Hz
    :type sample_rate: :class:`int`
    :param smoothing: Moving-average smoothing samples.
    :type smoothing: :class:`int`
    :return: Starting timestamp and interpolated data.
    """
    interpol_mag: NDArray = np.empty((1, 3))  # type: ignore
    interpol_gyr: NDArray = np.empty((1, 3))  # type: ignore
    interpol_acc: NDArray = np.empty((1, 3))  # type: ignore

    # Repeat for each axis
    for index in range(3):
        smoothed_acc = moving_average(
            recording.accelerometer[:, index], smoothing)
        smoothed_gyr = moving_average(
            recording.gyroscope[:, index], smoothing)
        smoothed_mag = moving_average(
            recording.magnetometer[:, index], smoothing)

        a, b = _get_moving_average_slice(smoothing)
        # Set the size for the output array
        acc_interpolated = _interpolate_via_timestamp(
            smoothed_acc, recording.timestamps[a:b], sample_rate)
        if len(interpol_acc) != len(acc_interpolated):
            interpol_mag.resize((len(acc_interpolated), 3))
            interpol_gyr.resize((len(acc_interpolated), 3))
            interpol_acc.resize((len(acc_interpolated), 3))

        interpol_mag[:, index] = _interpolate_via_timestamp(
            smoothed_mag, recording.timestamps[a:b], sample_rate)
        interpol_gyr[:, index] = _interpolate_via_timestamp(
            smoothed_gyr, recording.timestamps[a:b], sample_rate)
        interpol_acc[:, index] = acc_interpolated

    return recording.timestamps[0], np.concatenate(  # type: ignore
        (interpol_mag, interpol_gyr, interpol_acc), axis=1)


def _get_moving_average_slice(smoothing: int) -> tuple[int, int]:
    """Get the appropriate slice for the moving average window.

    This skips the first and last N/2 samples, biased towards the
    start of the window.

    :param smoothing: Moving-average smoothing samples.
    :type smoothing: :class:`int`
    :return: Slice for the moving average window.
    """
    if smoothing == 0:
        return 0, 1
    return (smoothing//2), (-smoothing//2) + 1
