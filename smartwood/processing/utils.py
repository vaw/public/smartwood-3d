"""Processing and data conversion utilities."""

import typing

import numpy as np
from scipy.constants import g as sp_g  # type: ignore
from scipy.spatial import transform as sp_transform  # type: ignore

from ..typing import NDArray

g = float(sp_g)  # type: ignore
NDTimestamps = np.ndarray[typing.Any, np.dtype[np.datetime64]]


@np.vectorize
def millig_to_meters_per_second_squared(data: float) -> float:
    """Convert milli-G values to meters per second squared.

    This class is decorated with ``np.vectorize`` and may be called
    with array-like arguments.

    :param data: The value or array to convert.
    :type data: :class:`float` | :obj:`numpy.ndarray`
    :return: The converted value or array.
    """
    return data * g / 1000.0


def quaternion_to_euler(quaternion: NDArray,
                        degrees: bool = False) -> NDArray:
    """Convert a quaternion to a set of Euler angles.

    The `quaternion` is expected to be an array of reals like
    ``[w, x, y, z]``. If `degrees` is ``True``, the angles will be
    returned in degrees, otherwise they will be returned in radians.

    :param quaternion: The quaternion to convert.
    :type quaternion: :obj:`numpy.ndarray`
    :param degrees: Whether to return the angles in degrees, by default
        ``False``.
    :type degrees: :class:`bool`
    :return: Corresponding Euler angles in the form ``[x, y, z]``.
    """
    # NOTE: AHRS (and the rest of this package) uses the WXYZ convention,
    # whereas Scipy uses XYZW, so we have to permute the quaternion.
    quat_xyzw = np.roll(quaternion, -1)  # type: ignore
    rot = sp_transform.Rotation.from_quat(quat_xyzw)  # type: ignore
    return rot.as_euler('xyz', degrees)  # type: ignore


def get_dt_from_timestamps(timestamps: NDTimestamps) -> float:
    """Calculate the time per sample from a sequence of timestamps.

    At least two timestamps are required.

    :param timestamps: The timestamps to calculate the time per sample
        from.
    :type timestamps: :obj:`numpy.ndarray`
    :return: The time per sample.
    :rtype: :class:`float`
    """
    if len(timestamps) < 2:
        raise ValueError('At least two timestamps required.')
    avg_delta = (timestamps[-1] - timestamps[0]) / len(timestamps)
    return avg_delta / np.timedelta64(1, 's')


def moving_average(data: NDArray, samples: int) -> NDArray:
    """Calculate a moving average over a given number of samples.

    :param data: Data to calculate moving average for.
    :type data: :obj:`numpy.ndarray`
    :return: Moving average of data.
    :rtype: :obj:`numpy.ndarray`
    """
    cumsum: NDArray = np.cumsum(data)  # type: ignore
    cumsum[samples:] = cumsum[samples:] - cumsum[:-samples]
    return cumsum[samples - 1:] / samples
