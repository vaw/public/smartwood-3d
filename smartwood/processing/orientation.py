"""Functions and types related to orientation estimation of the IMU."""

import typing

import numpy as np

from ahrs.filters import Madgwick  # type: ignore
from scipy.integrate import cumulative_trapezoid as sp_cumtrapz  # type: ignore

from ..recording import Recording
from ..typing import NDArray

from .utils import get_dt_from_timestamps, millig_to_meters_per_second_squared


def get_velocity(data: Recording) -> NDArray:
    """Calculate the local velocities for the IMU sensor.

    This uses trapezoidal integration to calculate the velocities in
    the sensor's local reference frame.

    :param data: Recording to calculate local velocities for.
    :type data: :class:`smartwood.Recording`
    :return: Array of local velocities.
    :rtype: :obj:`numpy.ndarray`
    """
    dt = get_dt_from_timestamps(data.timestamps)
    data_mps2 = millig_to_meters_per_second_squared(data.accelerometer)
    # Fill missing values with last valid value
    last_valid = [np.nan] * data_mps2.shape[1]
    for i in range(data_mps2.shape[0]):
        for j in range(data_mps2.shape[1]):
            if np.isnan(data_mps2[i, j]):
                data_mps2[i, j] = last_valid[j]
            else:
                last_valid[j] = data_mps2[i, j]
    return sp_cumtrapz(data_mps2, dx=dt, axis=0)  # type: ignore


def get_orientation(data: Recording, gain: float = 0.5,
                    initial: typing.Optional[NDArray] = None) -> NDArray:
    """Determine the orientation of the IMU for a given trigger block.

    :param data: Recording to calculate orientation for.
    :type data: :class:`smartwood.Recording`
    :param gain: Gain used for the Madgwick filter. Defaults to 0.033
        as per the AHRS algorithm's recommendation.
    :type gain: float
    :param initial: Initial orientation to use as a quaternion. If not
        provided, the algorithm may take up to two seconds to converge.
    :type initial: :obj:`numpy.ndarray`
    :return: Array of quaternions.
    :rtype: :obj:`numpy.ndarray`
    """
    # Convert IMU data to the formats required by the Madgwick filter

    dt = get_dt_from_timestamps(data.timestamps)
    # Convert milli-G to m/s^2
    acc_data = millig_to_meters_per_second_squared(data.accelerometer)
    # Convert deg/s to rad/s
    gyro_data = np.radians(data.gyroscope)
    # Convert micro-Tesla to milli-Tesla
    mag_data = data.magnetometer * 1e-3

    # AHRS algorithm (Madgwick)
    attitude = Madgwick(acc=acc_data, gyr=gyro_data, mag=mag_data,
                        gain=gain, Dt=dt, q0=initial)

    return attitude.Q  # type: ignore
