"""Custom iterator subclass supporting lookaheads.

Parsing the TXT sensor format often requires looking ahead 1-2 lines to
determine whether the next line is still part of the current block.

This iterator supports this by keeping a local cache of values,
allowing it to peek ahead without permanently consuming lines.
"""

import collections
import typing

T = typing.TypeVar('T')


class PeekableIterator(typing.Iterator[T]):
    """Iterator supporting peek-ahead."""

    def __init__(self, iterable: typing.Iterable[T]) -> None:
        self._iterator = iter(iterable)
        self._buffer: collections.deque[T] = collections.deque()
        self._exhausted: bool = False

    def __iter__(self) -> typing.Iterator[T]:
        return self

    def __next__(self) -> T:
        if self._buffer:
            return self._buffer.popleft()
        try:
            return next(self._iterator)
        except StopIteration as err:
            self._exhausted = True
            raise StopIteration from err

    def peek(self, offset: int = 0) -> typing.Optional[T]:
        """Peek ahead by `offset` elements.

        This will return an element occurring later in the iterable.
        Calling `iter()` after peeking continues where the iterator
        left off.

        An `offset` of 0 returns the current element without advancing
        the iterator. Negative offsets are not allowed.

        A `ValueError` is raised if the value at the given offset is
        beyond the end of the iterator.
        """
        if self._exhausted:
            raise RuntimeError('iterator is exhausted')
        if offset < 0:
            raise ValueError('offset may not be negative')
        # Expand the buffer until the desired element is contained
        try:
            for _ in range(len(self._buffer), offset + 1):
                self._buffer.append(next(self._iterator))
        except StopIteration:
            return
        # Return the target value
        return self._buffer[offset]
