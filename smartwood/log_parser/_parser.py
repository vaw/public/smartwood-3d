"""Main parser for reading sensor data."""

import datetime
import typing
import warnings

import numpy as np

from ..typing import FilePath
from ..dataset import Dataset
from ..recording import Recording
from ..utils import datetime_to_datetime64

from ._common import parse_timestamp, LoggingConfig, EngineeringData
from ._iterator import PeekableIterator


def parse_txt_tolerant(filepath: FilePath, encoding: str = 'utf-8',
                       ext_year: int = 0) -> Dataset:
    """A more fault-tolerant but less accurate TXT file parser.

    This only generates a single recording containing all values in
    the file.
    """
    with open(filepath, 'r', encoding=encoding) as file_:
        iterator = PeekableIterator(file_)

        # Read sensor name and software version
        line = next(iterator)
        sensor_str, version_str = line.split(';', maxsplit=1)
        _, sensor_name = [s.strip() for s in sensor_str.split(':', maxsplit=1)]
        _, version = [s.strip() for s in version_str.split(':', maxsplit=1)]

        if version != 'v1.7':
            raise ValueError(f'Unsupported TXT file version: {version}')

        dataset = Dataset(sensor_name)

        timestamps: typing.List[np.datetime64] = []
        mag: typing.List[tuple[float, float, float]] = []
        gyro: typing.List[tuple[float, float, float]] = []
        acc: typing.List[tuple[float, float, float]] = []

        trigger: typing.Optional[datetime.datetime] = None

        skipped = 0
        for line in iterator:
            if not line.startswith('VALUE'):
                continue

            try:
                words = [s.strip() for s in line.split(';')]
                timestamp = parse_timestamp(words[1:5], ext_year)
                val_mag = tuple(float(x[5:-2]) for x in words[5:8])
                value_gyro = tuple(float(x[5:-3]) for x in words[8:11])
                value_acc = tuple(float(x[5:-2]) for x in words[11:14])
            except ValueError:
                skipped += 1
                continue
            else:
                if len(val_mag) != 3 or len(value_gyro) != 3 or len(value_acc) != 3:
                    skipped += 1
                    continue
                timestamps.append(datetime_to_datetime64(timestamp))
                mag.append(val_mag)
                gyro.append(value_gyro)
                acc.append(value_acc)

            if trigger is None:
                trigger = timestamp

        if skipped > 0:
            print(f'Skipped {skipped} lines due to malformed data')

    np_timestamps = np.array(timestamps, dtype=np.datetime64)  # type: ignore
    np_magnetometer = np.array(mag, dtype=np.float32)  # type: ignore
    np_gyroscope = np.array(gyro, dtype=np.float32)  # type: ignore
    np_accelerometer = np.array(acc, dtype=np.float32)  # type: ignore

    recording = Recording(
        trigger, np_timestamps, np.concatenate(  # type: ignore
            (np_magnetometer, np_gyroscope, np_accelerometer), axis=1))

    dataset.add_recording(recording)

    return dataset


def parse_txt(filepath: FilePath, encoding: str = 'utf-8',
              ext_year: int = 0) -> Dataset:
    """Parse a sensor log file in TXT format.

    :param filepath: Path to the sensor log file.
    :type filepath: :class:`str` | :class:`pathlib.Path`
    :return: Dataset containing the sensor log file.
    :rtype: :class:`smartwood.Dataset`
    """
    with open(filepath, 'r', encoding=encoding) as file_:
        iterator = PeekableIterator(file_)

        # Read sensor name and software version
        line = next(iterator)
        sensor_str, version_str = line.split(';', maxsplit=1)
        _, sensor_name = [s.strip() for s in sensor_str.split(':', maxsplit=1)]
        _, version = [s.strip() for s in version_str.split(':', maxsplit=1)]

        if version != 'v1.7':
            raise ValueError(f'Unsupported TXT file version: {version}')

        dataset = Dataset(sensor_name)

        # Read "meas_started" block
        line = next(iterator)
        while line.startswith('static_values'):
            line = next(iterator)
        _, *words = [s.strip() for s in line.strip(';').split(';')]

        _ = LoggingConfig.from_words(words, ext_year)
        # Config
        _ = words

        # Read individual recordings

        while True:
            peeked = iterator.peek(2)
            if peeked is None or not peeked.startswith('trig_start_record'):
                break
            # 2 lines' worth of engineering data
            _ = EngineeringData.from_lines(iterator)
            # trig_start_record, etc.
            line = next(iterator)
            _, *words = [s.strip() for s in line.split(';')]
            _ = LoggingConfig.from_words(words, ext_year)
            # number_of_frames etc.
            _ = next(iterator)

            timestamps: typing.List[np.datetime64] = []
            mag: typing.List[tuple[float, float, float]] = []
            gyro: typing.List[tuple[float, float, float]] = []
            acc: typing.List[tuple[float, float, float]] = []

            trigger: typing.Optional[datetime.datetime] = None
            while True:
                peeked = iterator.peek(2)
                if peeked is None or peeked.startswith('rec_stopped'):
                    break
                line = next(iterator)
                words = [s.strip() for s in line.split(';')]

                type_ = words[0]
                timestamp = parse_timestamp(words[1:5], ext_year)
                timestamps.append(datetime_to_datetime64(timestamp))
                mag.append(tuple(float(x[5:-2]) for x in words[5:8]))
                gyro.append(tuple(float(x[5:-3]) for x in words[8:11]))
                acc.append(tuple(float(x[5:-2]) for x in words[11:14]))

                if type_ == 'VALUE' and trigger is None:
                    trigger = timestamp

            np_timestamps = np.array(  # type: ignore
                timestamps, dtype=np.datetime64)
            np_magnetometer = np.array(mag, dtype=np.float32)  # type: ignore
            np_gyroscope = np.array(gyro, dtype=np.float32)  # type: ignore
            np_accelerometer = np.array(acc, dtype=np.float32)  # type: ignore

            recording = Recording(
                trigger, np_timestamps, np.concatenate(  # type: ignore
                    (np_magnetometer, np_gyroscope, np_accelerometer), axis=1))

            dataset.add_recording(recording)

            # Consume the 3 rec_stopped lines
            for _ in range(3):
                _ = next(iterator)

        try:
            # Read engineering data
            _ = EngineeringData.from_lines(iterator)
            # Read meas_ended block
            line = next(iterator)
        except StopIteration:
            warnings.warn(f'Unexpected end of file: {filepath}')
            return dataset

        identifier, *words = line.split(';')
        assert identifier == 'meas_ended'
        _ = LoggingConfig.from_words(words)

        static_value_count = 0
        for line in iterator:
            if line.startswith('static_value'):
                static_value_count += 1
            else:
                break
        return dataset
