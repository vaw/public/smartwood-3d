"""I/O module handling the reading of exported sensor data via TXT.

The format specification and reference for this implementation can be
found in the `docs/` directory of this repository.
"""

from ._parser import parse_txt, parse_txt_tolerant

__all__ = [
    'parse_txt',
    'parse_txt_tolerant',
]
