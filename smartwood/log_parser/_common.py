"""Common message blocks used throughout the TXT format."""

import dataclasses
import datetime
import typing

__all__ = [
    'EngineeringData',
    'LoggingConfig',
    'parse_timestamp',
]


def parse_timestamp(words: typing.Sequence[str],
                    ext_year: int = 0) -> datetime.datetime:
    """Parse a Python datetime object from the given iterator.

    This variant currently only supports the MS Excel timestamp format.
    """
    assert len(words) == 4  # Only MS Excel format for now
    year, days, milliseconds = [int(s) for s in words if not ':' in s]
    try:
        hour, minute, second = [int(s) for s in words[2].split(':')]
    except (ValueError, IndexError) as err:
        raise ValueError(f'Unable to parse time: {words}') from err
    if year == 0:
        year = ext_year if ext_year != 0 else 1970
    timestamp = datetime.datetime(year, 1, 1)
    timestamp += datetime.timedelta(
        days=days,  # Sensor day 0 corresponds to Jan 1st
        hours=hour,
        minutes=minute,
        seconds=second,
        milliseconds=milliseconds)
    return timestamp


@dataclasses.dataclass(frozen=True)
class LoggingConfig:
    sensor_id: str
    timestamp: datetime.datetime
    recorded_data: str
    sample_rate_acc: float
    sample_rate_gyro: float
    sample_rate_ecomp: float
    mode: int

    @classmethod
    def from_words(cls, line: typing.List[str],
                   ext_year: int = 0) -> 'LoggingConfig':
        """Parse a LoggingConfig from the given line.

        The input list is mutated; any components belonging to the
        logging config and its parents are consumed.
        """
        sensor_id = line.pop(0)
        timestamp_words = [line.pop(0) for _ in range(4)]
        timestamp = parse_timestamp(timestamp_words, ext_year)
        recorded_data = line.pop(0)
        fps_acc, fps_gyro, fps_ecomp = [
            float(line.pop(0).split(':', maxsplit=1)[1]) for _ in range(3)]
        mode = int(line.pop(0).split(':', maxsplit=1)[1])
        return cls(sensor_id, timestamp, recorded_data,
                   fps_acc, fps_gyro, fps_ecomp, mode)


@dataclasses.dataclass(frozen=True)
class EngineeringData:
    # Line 1
    sensor_id: str
    temperature: float
    bat_after_bmm: float
    bat_after_16brf: float
    # Line 2
    bat_after_sleep: float
    vdd_after_bma_sleep_during_wft: typing.Optional[float]

    @classmethod
    def from_lines(cls, iterator: typing.Iterator[str]) -> 'EngineeringData':
        """Parse an EngineeringData from the given iterator.

        This operation consumes two lines from the iterator.
        """
        # First line
        words = [s.strip() for s in next(iterator).split(';')]
        _, sensor_id = words[0].split(':')
        temperature = float(words[1].split(':', maxsplit=1)[1][:-2])
        bat_after_bmm, bat_after_16brf = [
            float(s.split(':', maxsplit=1)[1][:-1]) for s in words[2:4]]
        # Second line
        words = [s.strip() for s in next(iterator).split(';')]
        bat_after_sleep, *vdd_after_bma_sleep_during_wft = [
            float(s.split(':', maxsplit=1)[1][:-1]) for s in words[1:3]]
        if vdd_after_bma_sleep_during_wft:
            vdd_after_bma_sleep_during_wft = vdd_after_bma_sleep_during_wft[0]
        else:
            vdd_after_bma_sleep_during_wft = None
        return cls(sensor_id, temperature, bat_after_bmm, bat_after_16brf,
                   bat_after_sleep, vdd_after_bma_sleep_during_wft)
