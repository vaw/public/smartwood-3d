"""Utilities and aliases for argument typing and type checking."""

import pathlib
import typing

import numpy as np

__all__ = [
    'FilePath',
    'NDArray',
    'TimeArray',
]

FilePath = typing.Union[str, pathlib.Path]
NDArray = np.ndarray[typing.Any, np.dtype[np.float64]]
TimeArray = np.ndarray[typing.Any, np.dtype[np.datetime64]]
