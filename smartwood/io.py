"""I/O module defining the SmartWood data format.

This module is responsible for translating between the Python data
types and the on-disk representations of the data.
"""

import csv
import datetime
import os
import pathlib
import typing
import warnings

import numpy as np

from .dataset import Dataset
from .recording import Recording
from .typing import FilePath
from .utils import (datetime64_from_string, datetime_to_string,
                    datetime64_to_string)

__all__ = [
    'load_dataset',
    'load_recording',
    'save_dataset',
    'save_recording',
]


def _get_csv_header() -> typing.List[str]:
    """Return the canonical headers in a SmartWood CSV file."""
    return [
        'Timestamp',
        'Magnetometer X [uT]',
        'Magnetometer Y [uT]',
        'Magnetometer Z [uT]',
        'Gyroscope X [deg/s]',
        'Gyroscope Y [deg/s]',
        'Gyroscope Z [deg/s]',
        'Accelerometer X [mg]',
        'Accelerometer Y [mg]',
        'Accelerometer Z [mg]',
    ]


def load_recording(filename: FilePath, *,
                   encoding: str = 'utf-8') -> Recording:
    """Load a SmartWood recording from disk.

    The header row will be validated to ensure compatibility of the
    dataset.

    :param filename: File from which to load the recording.
    :type filename: :class:`str` | :class:`pathlib.Path`
    :param encoding: Encoding to use when reading the file.
    :type encoding: :class:`str`
    :return: A SmartWood recording.
    :rtype: :class:`smartwood.Recording`
    """
    if not isinstance(filename, pathlib.Path):
        filename = pathlib.Path(filename)
    trigger = None
    try:
        _, ts_str = filename.stem.split('_', maxsplit=1)
        ts_str, *_ = ts_str.rsplit('.', maxsplit=1)
        trigger = datetime.datetime.strptime(ts_str, '%Y-%m-%d_%H-%M-%S')
    except ValueError:
        warnings.warn(
            f'Unable to determine timestamp from filename: {filename.stem}')

    with open(filename, 'r', newline='', encoding=encoding) as file_:
        reader = csv.reader(file_)
        header = next(reader)
        if header != _get_csv_header():
            raise ValueError('File header does not match SmartWood format: '
                             f'{header}')

        timestamps: typing.List[np.datetime64] = []
        magnetometer: typing.List[tuple[float, float, float]] = []
        gyroscope: typing.List[tuple[float, float, float]] = []
        accelerometer: typing.List[tuple[float, float, float]] = []

        for row in reader:
            timestamps.append(datetime64_from_string(row[0]))
            magnetometer.append(tuple(map(float, row[1:4])))
            gyroscope.append(tuple(map(float, row[4:7])))
            accelerometer.append(tuple(map(float, row[7:10])))

    np_timestamps = np.array(  # type: ignore
        timestamps, dtype=np.datetime64)
    np_magnetometer = np.array(  # type: ignore
        magnetometer, dtype=np.float32)
    np_gyroscope = np.array(  # type: ignore
        gyroscope, dtype=np.float32)
    np_accelerometer = np.array(  # type: ignore
        accelerometer, dtype=np.float32)

    if trigger is None:
        trigger = timestamps[0]

    return Recording(trigger, np_timestamps, np.concatenate(  # type: ignore
        (np_magnetometer, np_gyroscope, np_accelerometer), axis=1))


def save_recording(recording: Recording, filename: FilePath, *,
                   encoding: str = 'utf-8') -> None:
    """Write the given SmartWood recording to disk.

    :param recording: A SmartWood recording to save.
    :type recording: :class:`smartwood.Recording`
    :param filename: File into which to save the recording.
    :type filename: :class:`str` | :class:`pathlib.Path`
    :param encoding: Encoding to use when writing the file.
    :type encoding: :class:`str`
    """
    with open(filename, 'w', newline='', encoding=encoding) as file_:
        writer = csv.writer(file_)
        writer.writerow(_get_csv_header())
        for timestamp, mag, gyro, acc in zip(
                recording.timestamps, recording.magnetometer,
                recording.gyroscope, recording.accelerometer):
            row = datetime64_to_string(timestamp), *mag, *gyro, *acc
            writer.writerow(row)


def load_dataset(directory: FilePath, *,
                 encoding: str = 'utf-8') -> Dataset:
    """Load a SmartWood dataset from disk.

    :param directory: Directory containing the dataset files.
    :type directory: :class:`str` | :class:`pathlib.Path`
    :param encoding: Encoding to use when reading the files.
    :type encoding: :class:`str`
    :return: A SmartWood dataset.
    :rtype: :class:`smartwood.Dataset`
    """
    if not isinstance(directory, pathlib.Path):
        directory = pathlib.Path(directory)
    _, name, timestamp = directory.stem.split('_', maxsplit=2)
    _ = timestamp
    dataset = Dataset(name)
    for file_ in directory.iterdir():
        if file_.suffix == '.csv':
            dataset.add_recording(load_recording(file_, encoding=encoding))
    return dataset


def save_dataset(dataset: Dataset, directory: FilePath, *,
                 encoding: str = 'utf-8') -> None:
    """Save the given SmartWood dataset to disk.

    Note that unlike :func:`smartwood.load_dataset`, this function
    takes in the parent directory containing the dataset directory.
    The directory itself will be created based on the dataset contents.

    :param dataset: A SmartWood dataset to save.
    :type dataset: :class:`smartwood.Dataset`
    :param directory: Directory into which to save the dataset.
    :type directory: :class:`str` | :class:`pathlib.Path`
    :param encoding: Encoding to use when writing the files.
    :type encoding: :class:`str`
    """
    if not isinstance(directory, pathlib.Path):
        directory = pathlib.Path(os.path.abspath(directory))
    directory.mkdir(exist_ok=True)
    # TODO: This start TS is wrong
    start_str = datetime64_to_string(dataset.get_recording(0).timestamps[0])
    dataset_dir = directory / f'sensor_{dataset.sensor_name}_{start_str}'
    if dataset_dir.exists():
        raise ValueError('Dataset directory already exists')
    dataset_dir.mkdir()
    # TODO: Write metadata
    for recording in dataset.recordings():
        trigger_str = datetime_to_string(recording.trigger)
        filename = dataset_dir / f'trigger_{trigger_str}.csv'
        save_recording(recording, filename, encoding=encoding)
