"""Utility module for commonly reused functions."""

import datetime

import numpy as np

__all__ = [
    'datetime_from_string',
    'datetime_to_string',
    'datetime64_from_string',
    'datetime64_to_string',
    'datetime64_to_datetime',
    'datetime_to_datetime64',
]

NP_EPOCH = np.datetime64('1970-01-01T00:00:00Z')


def datetime_from_string(string: str) -> datetime.datetime:
    """Parse a SmartWood timestamp to a datetime.datetime."""
    return datetime.datetime.strptime(string, '%Y-%m-%d_%H-%M-%S.%f')


def datetime64_from_string(string: str) -> np.datetime64:
    """Parse a SmartWood timestamp to a numpy.datetime64."""
    return np.datetime64(datetime_from_string(string))


def datetime_to_string(datetime_: datetime.datetime) -> str:
    """Convert a datetime.datetime to a SmartWood timestamp."""
    return datetime_.strftime('%Y-%m-%d_%H-%M-%S.%f')


def datetime64_to_string(np_datetime: np.datetime64) -> str:
    """Convert a numpy.datetime64 to a SmartWood timestamp."""
    return datetime_to_string(datetime64_to_datetime(np_datetime))


def datetime64_to_datetime(np_datetime: np.datetime64) -> datetime.datetime:
    """Convert a numpy.datetime64 to a datetime.datetime."""
    timestamp = float((np_datetime - NP_EPOCH) / np.timedelta64(1, 's'))
    return datetime.datetime.utcfromtimestamp(timestamp)


def datetime_to_datetime64(datetime_: datetime.datetime) -> np.datetime64:
    """Convert a datetime.datetime to a numpy.datetime64."""
    return np.datetime64(datetime_)
