import typing

from .recording import Recording

__all__ = [
    'Dataset',
]


class Dataset:
    """Sensor logging dataset.

    Datasets are collections of multiple sensor recordings using the
    same sensor configuration. When downloading bulk data from a
    SmartWood sensor, this data generally translates into a single
    dataset.
    """

    def __init__(self, sensor_name: str) -> None:
        self.sensor_name: str = sensor_name
        self.metadata: typing.Dict[str, typing.Any] = {}
        self._recordings: typing.List[Recording] = []

    @property
    def num_recordings(self) -> int:
        """Return the number of recordings in the dataset."""
        return len(self._recordings)

    def recordings(self) -> typing.Iterator[Recording]:
        """Iterate over the recordings in the dataset."""
        return iter(self._recordings)

    def get_recording(self, index: int) -> Recording:
        """Return the recording at the given index."""
        return self._recordings[index]

    def add_recording(self, recording: Recording) -> None:
        """Add the given recording to the dataset."""
        self._recordings.append(recording)
