import datetime
from typing import Any, Callable, TypeVar

import numpy as np

from .utils import datetime64_to_datetime
from .typing import NDArray

__all__ = [
    'Recording',
]

T = TypeVar('T')
CallableT = Callable[..., T]


def _require_data(func: CallableT[T]) -> CallableT[T]:
    """Decorator for class methods that require data to execute.

    This decorator will prevent the decorated method from running if
    the class instance has no data attributes set.
    """

    def decorator(self: 'Recording', *args: Any, **kwargs: Any) -> T:
        try:
            _ = getattr(self, '_timestamps')[0]
            _ = getattr(self, '_data')[0]
        except (IndexError, AttributeError) as err:
            raise ValueError('Recording has no data') from err
        return func(self, *args, **kwargs)

    return decorator


class Recording:
    """An individual sensor recording.

    Recordings are started by a trigger condition controlled by the
    sensor, such as a sharp impact or other sudden change in
    acceleration. It is then recorded for a period of time until the
    acceleration changes fall below another shutdown-threshold.
    """

    def __init__(self, trigger: datetime.datetime,
                 timestamps: np.ndarray[Any, np.dtype[np.datetime64]],
                 data: NDArray) -> None:
        self._timestamps = timestamps
        self._data = data
        self._trigger = trigger

    @property
    @_require_data
    def start(self) -> datetime.datetime:
        """Return the timestamp of the first sample."""
        return datetime64_to_datetime(self._timestamps[0])

    @property
    @_require_data
    def trigger(self) -> datetime.datetime:
        """Return the approximate of the trigger point.

        This time is calculated from the first sample that exceeds the
        trigger threshold, and could therefore be up to 1 sample
        behind the actual trigger condition.
        """
        return self._trigger

    @property
    @_require_data
    def end(self) -> datetime.datetime:
        """Return the timestamp of the last sample."""
        return datetime64_to_datetime(self._timestamps[-1])

    @property
    @_require_data
    def duration(self) -> datetime.timedelta:
        """Return the duration of the recording."""
        return self.end - self.start

    @property
    def timestamps(self) -> np.ndarray[Any, np.dtype[np.datetime64]]:
        """Return the timestamps of the samples."""
        return self._timestamps

    @property
    def magnetometer(self) -> NDArray:
        """Return the X, Y, and Z axis magnetometer data."""
        return self._data[:, 0:3]

    @property
    def gyroscope(self) -> NDArray:
        """Return the X, Y, and Z axis gyroscope data."""
        return self._data[:, 3:6]

    @property
    def accelerometer(self) -> NDArray:
        """Return the X, Y, and Z axis accelerometer data."""
        return self._data[:, 6:9]
