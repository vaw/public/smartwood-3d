"""Utility script running all individual SmartWood scripts in order."""

import pathlib
import subprocess
import sys
import typing

PYTHON = sys.executable
TOOLS_DIR = pathlib.Path(__file__).parent / 'tools'


def _build_cmd(script_name: str, *args: str) -> typing.List[str]:
    """Shorthand for generating commands to run SmartWood scripts."""
    cmd = [PYTHON, str(TOOLS_DIR / script_name)]
    cmd.extend(args)
    return cmd


def _recursive_rm(path: pathlib.Path) -> None:
    """Recursively remove a directory."""
    if path.is_dir():
        for child in path.iterdir():
            _recursive_rm(child)
        path.rmdir()
    else:
        path.unlink(missing_ok=True)


def main(path: typing.Union[str, pathlib.Path], smoothing: int,
         start: int, gravity: float) -> None:
    """Run all SmartWood scripts in order."""

    # Clear output directory
    if not isinstance(path, pathlib.Path):
        path = pathlib.Path(path)
    output_dir = path.parent.absolute() / f'{path.stem}_processing'
    # Clear output directory
    _recursive_rm(output_dir)
    # Create output directory
    output_dir.mkdir(exist_ok=False)

    print('(1/4) Converting sensor data to SmartWood format...')
    subprocess.check_call(_build_cmd(
        'raw_to_smartwood.py',
        str(path),
        '--encoding', 'utf-16le',
        '--output', str(output_dir),
        '--tolerant',
    ))

    print('(2/4) Stitching sensor data...')
    # Get first directory in output directory
    try:
        sensor_dir = next(output_dir.iterdir())
    except StopIteration:
        print('No data found in output directory')
        return
    subprocess.check_call(_build_cmd(
        'stitch_recordings.py',
        str(sensor_dir),
        '--smoothing', str(smoothing),
    ))

    print('(3/4) Determining orientation...')
    subprocess.check_call(_build_cmd(
        'ahrs_orientation.py',
        str(output_dir / 'stitched.csv'),
    ))

    print('(4/4) Integrating velocity...')
    subprocess.check_call(_build_cmd(
        'velocity.py',
        str(output_dir / 'orientation.csv'),
        '--start', str(start),
        '--gravity', str(gravity),
    ))

    print('done')


if __name__ == '__main__':

    # Path containing the raw sensor data
    PATH = 'Li_VP1_204_green_RawData.txt'

    # Smoothing window size in samples
    SMOOTHING = 10

    # Start offset for velocity integration in samples (100 samples = 1 second)
    START = 200

    # Gravitational correction factor
    # (0.0 = no correction, 0.01 = +1%, -0.02 = -2%)
    GRAVITY_CORRECTION = 0.0

    main(PATH, SMOOTHING, START, GRAVITY_CORRECTION)
